import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
 
import Film from './components/Film.vue'
import Char from './components/Char.vue'
import Films from './components/Films.vue'
Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    // dynamic segments start with a colon
    { path: "/", component: Films },
    { path: '/Film/:id', component: Film },
    { path: '/Char/:id', component: Char }
  ]
})

new Vue({
  render: h => h(App),
  router
}).$mount('#app')
